/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 6 Lab 1 - Tree Lab
Tree, dynamic left-child/right sibling implementation
*/

#ifndef TREE_H
#define TREE_H

#include "node.h"
#include "ldeque.h"
#include <fstream>
#include <algorithm> 
#include <string>  
#include <functional>
#include <cctype>

using namespace std;

class Tree{
private:
  Node<int,string>* root;       // Pointer to root tree node
  int size;                // Number of elements in tree
  bool loaded; //Stores if you've already loaded in a set of words from a file
  //LDeck<string> storedWords; // A deque for the imported words

  //private functions
  void clearhelp(Node<int, string>*);
  Node<int,string>* inserthelp(Node<int,string>*, const int&, const string&);
  string findhelp(Node<int, string>*, const int&) const;
  void printhelp(Node<int, string>*, int) const;
  
  Node<int,string>* inserthelpElem(Node<int,string>*, const int&, const string&);
  int findElemhelp(Node<int, string>*, const string&) const;
  void deckInhelp(LDeck<string>);
  void findWordhelp(Node<int, string>* root, const string& elemVal, string& word, int level, bool &valid) const;
  
  void insertWord(string word);
  Node<int, string>* inserthelpWord(Node<int, string>*, const int&, const string&, Node<int,string>*& builder);

public:
  Tree() // Constructor 
    {
		root = NULL;
		size = 0;
		loaded = 0;
	}

  ~Tree()
  {
	  //clearhelp(root);
	  size=0;
  }      // Destructor

  void fileIn(string filename) //Reads a list of words from a given filename, builds the tree with it if it's the first file load, checks against the already built tree afterwards
  {	  

	  LDeck<string> storedWords; // A deque for the imported words
	  string line;
	  if (loaded == 0) //First load
	  {
		  ifstream myfile (filename); //Open the word list
		  if (myfile.is_open()) 
		  {
			cout << "Loading words from file..." << endl;
			while ( getline (myfile,line) ) // Grabs each line from the text file, uppercases them, stores them in a deck
			{
				std::transform(line.begin(), line.end(), line.begin(), ptr_fun<int, int>(toupper)); //Transforms the incoming line to upper case
				//cout << "Putting the word: " << line << " in the deck." << endl; //TEMP -- prints when each word is pulled from text file
				storedWords.enqueue(line); //Stores each incoming word in the deck
			}
			myfile.close(); // Closes the file
			storedWords.sortLtH(); //Sort the deck
			deckInhelp(storedWords); //Passes the deck to a helper function to build the tree
			loaded = 1; //Indicates that we've loaded the deck
		  }

		  else
		  {
			  cout << "Unable to open file.";
			  abort(); //Can't open the file, print an error and kill program
		  }
	  }
	  else
	  {		  
		  ifstream myfile (filename); //Open the word list
		  cout << "Loading list to check from file..." << endl;
		  if (myfile.is_open()) 
		  {
			while ( getline (myfile,line) ) //Grabs each line from a text file, uppercases them. then checks if they're in the tree
			{
				std::transform(line.begin(), line.end(), line.begin(), ptr_fun<int, int>(toupper)); //Transforms the incoming line to upper case
				findWord(line); //Checks if the word is already in the tree
			}
			myfile.close(); //Close the file
		  }

		  else
		  {
			  cout << "Unable to open file."; 
			  abort();//Can't open the file, print an error and kill the program
		  }
	  }
	 
  }
  
  
  void clear()   // Empty out the tree
    { 
		clearhelp(root); 
		root = NULL; 
		size = 0; 
    }

  
  // Insert a record into the tree.
  // k int value of the record.
  // string The record to insert.
  void insert(const int& k, const string& string) 
  {
	root = inserthelpElem(root, k, string);
    size++;
  }
  

  // Return Record with int value k, NULL if none exist.
  // k: The int value to find. */
  // Return some record matching "k".
  // Return true if such exists, false otherwise. If
  // multiple records match "k", return an arbitrary one.
  string find(const int& k) const 
  {
	  string temp = findhelp(root, k);
	  if (temp.empty())
	  {
		  cout << "int: " << k << " Not found" << endl;
		  return "";
	  }
	  return findhelp(root, k); 
  }
  
  //As above, but returns the a int when an element is searched, rather than the element when a int is searched
  int findElem(const string& elemVal) const
  { 
	  int temp = findElemhelp(root, elemVal); 
	  if (temp == 0)
	  {
		  cout << "Element " << elemVal << " not found";
		  return 0;
	  }
	  return findElemhelp(root, elemVal);
  }

  //Searches for a word in the tree, prints if the word if found in the tree or not.
  void findWord(const string& elemVal) const
  {
	  //cout << "Loking for: " << elemVal <<endl;//TEMP
	  string word;
	  bool valid = 0; 
	  findWordhelp(root, elemVal, word, 0, valid);
	  if (valid == 0)
	  {
		  cout << "Element: " << elemVal << " not found in tree." << endl;
	  }
	  else
	  {
		  cout << "Element: " << elemVal << " found in tree." << endl;
	  }
  }

  // Return the number of records in the dictionary.
  int length() 
  {
	  return size;
  }
  /* Print's not working for non-binary tree
  void print() const
	{ // Print the contents of the Tree
		if (root == NULL) cout << "The BST is empty.\n";
		else printhelp(root, 0);
	}
	*/
  virtual int length() const 
  {
	return size;
  }
    
  //Testing, shows the element of the root node. 
  string findRoot() const
  {
	  string it;
	  it = root->element;
	  return it;
  }
  
  //Testing, shows the elements of the root's two immediate children.
  void findRootchild(string &le, string &ri) const
  {
	  Node<int,string>* temp;
	  temp = root->getLeft();
	  le = temp->element;
	  temp = root->getRight();
	  ri = temp->element;
  }

};

// Clean up tree
void Tree::
clearhelp(Node<int,string>* root) 
{
  if (root == NULL) return;
  clearhelp(root->getLeft());
  clearhelp(root->getRight());
  delete root;
}


// Insert a node into the BST, returning the updated tree
Node<int,string>* Tree::
	inserthelp(Node<int,string>* root, const int& k, const string& elem)
{
  if (root == NULL)  // Empty tree: create node
  {
	  return new Node<int,string>(k, elem, NULL, NULL);
  }
  if (k < root->getKey()) //If the int of the new node is less than the root's
  {
	  root->setLeft(inserthelp(root->getLeft(), k, elem)); //Runs this method again, with the left child of the current node as the new root
  }
  else root->setRight(inserthelp(root->getRight(), k, elem)); //int of the new node is greater than or equal to the root's, so run this method again with the right child as the new root
  return root;       // Return tree with node inserted
}

//As above, but compares the values of the element, rather than the int
Node<int,string>* Tree::
	inserthelpElem(Node<int,string>* root, const int& k, const string& elem)
{
  if (root == NULL)  // Empty tree: create node
  {
	  return new Node<int,string>(k, elem, NULL, NULL);
  }
  if (elem < root->getElement()) //If the element of the new record is less than the root's
  {
	  root->setLeft(inserthelp(root->getLeft(), k, elem));
  }
  else root->setRight(inserthelp(root->getRight(), k, elem));
  return root;       // Return tree with node inserted
}


// Find a node with the given int value
string Tree::findhelp(Node<int,string>* root,
                              const int& k) const {
  if (root == NULL) return ""; //NULL;          // Empty tree
  if (k < root->getKey())
    return findhelp(root->getLeft(), k);   // Check left
  else if (k > root->getKey())
    return findhelp(root->getRight(), k);  // Check right
  else return root->getElement();  // Found it
}

// Find a node's int with the given element
int Tree::
	findElemhelp(Node<int,string>* root, const string& elemVal) const 
{
  if (root == NULL) return NULL;          // Empty tree
  if (elemVal < root->getElement())
    return findElemhelp(root->getLeft(), elemVal);   // Check left
  else if (elemVal > root->getElement())
    return findElemhelp(root->getRight(), elemVal);  // Check right
  else return root->getKey();  // Found it
}

// Find if a word is in the tree
void Tree::
findWordhelp(Node<int, string>* root, const string& elemVal, string& word, int level, bool &valid) const
{
	if (root == NULL) // Empty tree, or you've hit the end of the tree
	{
		//cout << "Hit a dead end. Does the word built match the search?" << endl; //TEMP
		if (word.compare(elemVal) == 0) // If the word string you've built matches the word you're looking for, return true
		{
			valid = 1;
			return;
		}
		else // Word you've built doesn't match the word you're looking for, return false
		{
			
			valid = 0;
			return;
		}
	}

	char c = elemVal.at(level); //Pulls the character from the string
	string str(1,c); // holds a character from the specified position from searched string

	if (str == root->getElement()) //The root's character matches the searched string's at the specified level/position
	{
		word.append(root->getElement()); //Add the character to the string being built
		if (level+1 == elemVal.size()) //Hit the end of the word we're searching for, can't progress a level
		{
			if (word.compare(elemVal) == 0) // If the word string you've built matches the word you're looking for, return true
			{
				valid = 1;
				return;
			}
			else // Word you've built doesn't match the word you're looking for, return false
			{
				valid = 0;
				return;
			}
		}
		else
		{
				findWordhelp(root->getLeft(), elemVal, word, level+1, valid);// Check again, at the next level 
		}
	}
	else // Didn't match, check the sibling
	{
		findWordhelp(root->getRight(), elemVal, word, level, valid);// Check again, at the sibling, at the same level

	}

	return;
}

	// Adds the imported words to the tree
	void Tree::
	deckInhelp(LDeck<string> inDeck)  
	{
		if (inDeck.length() == 0) 
		{
			cout << "Empty Deck detected while decking in." << endl;
			return; // Empty deck, print an error
		}
	  

	  for (int i = 0; inDeck.length()>0; i++) //Operates on each card in the deck
	  {
		string hold; //Stores the value of a card
		hold = inDeck.dequeue(); //Pulls a card from the deck
		cout << "Inserting word #" << i+1 << ": " << hold << " into the tree." << endl; //TEMP - Prints out each word being added to the tree.
		insertWord(hold); //Passes the word to a helper function
	  }    
	  cout << endl; // TEMP - formatting
	}

//Adds a word to the tree, one character at a time
void Tree::
	insertWord(string word)
{	
	Node<int,string>* builder = root; //Stores the location of the last character inserted into the tree, starts at the root
	int length = word.length(); //Stores the length of the word to be inserted

		for (int x = 0; x<length; x++) //Operates on each character in the word
		{
			Node<int,string>* subroot = root; //points to the root, refreshes for each character
			char c = word.at(x); //Pulls the character from the incoming string
			string s(1,c); // Turns the character into a string
			if (x==0) //First letter of each word
			{
				root = inserthelpWord(root,x,s, builder); // Inserts the string containing the single character being inserted, only for the first letter of each word
			}
			else //Already put in the first letter, skip to where we left off
			{
				subroot = inserthelpWord(builder,x,s,builder); // Inserts the string containing the single character being inserted, starting at where builder left off
			}
			size++; //increments the size
			//cout << "Putting " << s << " in the tree" << endl; // TEMP -- Prints when a letter is added to the tree
		}

}

//Inserts letters into the tree, stores where the last letter you inserted is
Node<int,string>* Tree::
	inserthelpWord(Node<int,string>* root, const int& k, const string& elem, Node<int,string>*& builder)
{
  if (root == NULL)  // Empty tree: create node
  {
	  builder = new Node<int,string>(k, elem, NULL, NULL); //Creates a node, sets builder to point to it
	  return builder; //returns the created node
  }
  if (k > root->getKey()) //If the int of the new record is greater than the root's, that is to say, if the character being inserted is in a position after the root, and thus a child.
  {
	  root->setLeft(inserthelpWord(root->getLeft(), k, elem, builder));//Runs this method again, with the left child of the current node as the new root
  }
  else
  {
	  if (elem == root->getElement()) //Found a copy of the element being inserted with the same int, that is to say at the same position
	  {
		  builder = root; //Sets the builder to point to this node, no need to make a new node
		  size--; //Compensates for the size increment in the insert, we didn't make a new node so no need to up the size
		  return root;
	  }
	  else
	  {
		  root->setRight(inserthelpWord(root->getRight(), k, elem, builder)); // If the int is equal or less than the root. If it's equal, it's a sibling, if it's less - it shouldn't be.
		  if (k < root->getKey()) cout << "You shouldn't get this, int less than root" << endl;
	  }
	  
  }
  return root;       // Return tree with node inserted
}

#endif