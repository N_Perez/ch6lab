/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 6 Lab 1 - Tree Lab
tree nodes
*/

#ifndef NODE_H
#define NODE_H
#define NULL 0

template <typename Key, typename E> class Node {
public:
  E element;      // Value for this node
  Key k;		// The node's key
  Node *left;        // Pointer to left child node in tree
  Node *right;        // Pointer to right sibling node in tree
  

  //Destructor
  virtual ~Node(){  }

  // Constructors
  Node (const Key& inK, const E& elemval, Node* leftval =NULL, Node* rightval =NULL) //Constructor with values
    {
		k = inK;
		element = elemval;  
		left = leftval; 
		right = rightval;
	}
  Node (Node* leftval =NULL, Node* rightval =NULL) //Constructor without values
	{
	  left = leftval; 
	  right = rightval;
	}
  
  virtual E& getElement() //Return the node's element
  {
	  return element;
  }
  virtual void setElement(const E& elemVal) //Set the node's element
  {
	  element = elemVal;
  }
  
  virtual Key& getKey() //Return the node's key
  {
	  return k;
  }
  virtual void setKey(const Key& inK) //Set the node's key
  {
	  k = inK;
  }

  virtual Node* getLeft() const //Return the node's left child
  {
	  return left;
  }
  virtual void setLeft(Node* changedLeft) //Set the node's left child
  {
	  left = changedLeft;
  }
  
  virtual Node* getRight() const //Return the node's right child
  {
	  return right;
  }
  virtual void setRight(Node* changedRight) //Set the node's right child
  {
	  right = changedRight;
  }

  virtual bool isLeaf() //Is this a leaf node; that is to say, are both its children null?
  {
	  return (left == NULL) && (right == NULL);
  }
  
};

#endif