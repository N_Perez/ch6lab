/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 6 Lab 1 - Tree Lab
deque
*/


#ifndef LDEQUE_H
#define LDEQUE_H

#include "link.h"



template <typename E> class LDeck{
private:
  Link<E>* front;       // Pointer to front queue node
  Link<E>* rear;        // Pointer to rear queue node
  int size;                // Number of elements in queue

  void swapLinkVal (Link<E>* linkOne, Link<E>* linkTwo)
  {
		  E linkOneValue = linkOne->element; // Holds the element for link one
		  E linkTwoValue = linkTwo->element; // Holds the element for link two

		  linkOne->element= linkTwoValue; //Swaps the elements in link one and two
		  linkTwo->element= linkOneValue;
  }

public:
  LDeck() // Constructor 
    {
		front = rear = new Link<E>(); // Creates a new node, sets front and rear pointers to point at it
		size = 0;
	}
  /* Destructor/clear() is busted
  ~LDeck() { clear(); delete front; }      // Destructor

  void clear() //
  {           // Clear queue
	  if (front == NULL)
	  {
		  size = 0;
		  return;
	  }
	  else
	  {
		  while (front->next != NULL) // Delete each link node 
		  {
			  Link<E>* tempLink = front->next; // Hold soon-to-be deleted link
			  front->next = tempLink->next;       // Advance front
			  if (rear == tempLink)
			  {
				  rear = front; // about to delete last link
				  front->next = NULL;
			  }
			  delete tempLink;                    // Delete link
		  }
		  size = 0;
	  }
  }
  */
  void enqueue(const E& it) // Put element on rear
  { 
    rear->next = new Link<E>(it, NULL, NULL); //Sets the rear 'next' pointer to point to a new link
	Link<E>* temp = rear; //creates a temporary pointer that indicates what is now 2nd from rear
    rear = rear->next; //sets the 'rear' pointer to point to the new link
	rear->prev= temp; // sets the new link's previous pointer to the one before it
    size++; 
  }

  E dequeue() // Remove element from front
  {
    //assert(size != 0, "Queue is empty"); //how to into asserts
	  if (size == 0)
	  {
		  cout << "Deque is empty. Invalid dequeue" << endl;
		  abort();
	  }

	  else
	  {
		E it = front->next->element;  // Store dequeued value
		Link<E>* ltemp = front->next; // Hold dequeued link
		front->next = ltemp->next;       // Advance front
		if (rear == ltemp) 
			{
				rear = front; // Dequeue last element
				front->next=NULL; 
			}
		delete ltemp;                    // Delete link
		size --;
		return it;                       // Return element value
	  }
  }

  //deque functions, but I'll be using "deck" throughout 

  void endeck(const E& it) // Put element on front
  { 
	  if (front->next == NULL) // No elements in the deck
	  {
		  front->next = new Link<E>(it, NULL, NULL); //Creates a new link, sets the front's 'next' pointer to it
		  front->next->prev = front; //Sets the new link's 'prev' pointer to point to the front link
		  rear = front->next;
	  }
	  else
	  {
		  front->next->prev = new Link<E>(it, NULL, NULL); //Sets the first element's 'previous' pointer to point to a new link
		  Link<E>* temp = front->next; //creates a temporary pointer that indicates what is going to be 2nd from front
		  front->next = temp->prev; // Sets the front's 'next' pointer to point to the new link
		  front->next->next = temp; // Sets the new link's 'next' pointer to point to what is now the second element from front
		  front->next->prev = front; //Sets the new link's 'prev' pointer to point to the front link
	  }
    
	
    size++; 
  }

  E dedeck() // Remove element from back
  {
    //assert(size != 0, "Queue is empty"); 
	  if (size == 0)
	  {
		  cout << "Deque is empty. Invalid dedeck." << endl;
		  abort();
	  }

	  else
	  {
		E it = rear->element;  // Store dedecked value
		Link<E>* ltemp = rear; // Hold dedecked link
		rear = ltemp->prev;       // Advance rear
		
		if (front->next == ltemp) 
			{
				front->next = NULL; // Dedecked last element, empty the front's next pointer 
				rear = front;
			}
			
		delete ltemp;                    // Delete link
		size --;
		return it;                       // Return element value
	  }
  }


  void sortLtH()
  {
	  if (size == 0)
	  {
		  cout << "Deque is empty. Invalid sort." << endl;
		  abort();
	  }
	  else
	  {
		  bool unsorted = true;
		  cout << "Sorting..." << endl;
		  while (unsorted == true)
		  {
			  Link<E>* tempFront = front->next; //Points to the first link in the deck
			  Link<E>* tempBack = tempFront->next; //points to the second link in the deck
			  // These two pointers create a pair to perform bubble sort with
			  //Link<E>* tempLink;
			  bool change = false;//tracks if there was a change this pass through

			  while (tempFront->next != NULL) //While your front number isn't the last number in the deck
			  {
				if (tempFront->element > tempBack->element) //Is the element of the first node greater than the element of the second node?
				{
					//Yes, swap their values, say we made a change. Our front link now has the back link's value, and vice versa.
					 swapLinkVal(tempFront, tempBack);
					 change = true; // Indicates we did something this passthrough
					 tempFront = tempFront->next;
					 tempBack = tempBack->next;
				}
				else
				{  //No. Our back link becomes our front, the one after that becomes back.
					tempFront = tempFront->next;
					tempBack = tempBack->next;
				}
			  }
			  if (change == false)
			  {
				  unsorted = false; //Finished sorting

			  }
		  }

	  }
  }

  //testing functions
  
  const E& frontValue() const 
  { // Get front element	  
    //assert(size != 0, "Queue is empty");
	  if (size == 0)
	  {
		  cout << "Deque is empty. Invalid frontValue." << endl;
		  abort();
		  ;
	  }
	  else
	  {
		return front->next->element;
	  }
  }
  const E& rearValue() const 
  { // Get rear element	  
    //assert(size != 0, "Queue is empty");
	  if (size == 0)
	  {
		  cout << "Deque is empty. Invalid rearValue" << endl;
		  abort();
		  return 0;
	  }
	  else
	  {
		return rear->element;
	  }
  }
  


  virtual int length() const 
  {
	return size;
  }
  
  void readItBack()
  {
	  while (size > 0)
	  {
		  cout << dequeue() <<endl;
	  }
	  return;
  }
};

#endif